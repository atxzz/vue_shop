## [点我访问项目](http://47.108.84.237)
## 本项目是网站的后台管理系统，登录到后台，可以对数据进行增删改查。
![登录](./src/assets/poster.png)
![列表](./src/assets/cont.png)
## Project setup
```
npm install
```

## Compiles and hot-reloads for development
```
npm run serve
```

## Compiles and minifies for production
```
npm run build
```
