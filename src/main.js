import Vue from 'vue';
import App from './App.vue';
import router from './router';
import './plugins/element';
import axios from 'axios';
import './assets/css/global.css';  //全局样式
import './assets/fonts/iconfont.css';  //字体图标
import TreeTable from 'vue-table-with-tree-grid';

import VueQuillEditor from 'vue-quill-editor';
import 'quill/dist/quill.core.css';
import 'quill/dist/quill.snow.css';
import 'quill/dist/quill.bubble.css';
Vue.use(VueQuillEditor);

axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/';  //请求的根路径
axios.interceptors.request.use(config => {   //请求拦截器
  config.headers.Authorization = window.sessionStorage.getItem('token');  //为请求头对象添加Token验证的Authorization字段
  return config;
});
Vue.prototype.$http = axios;
Vue.component('tree-table',TreeTable);

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
